# name: name-mask
# about: Masks usernames under most_liked_section also logout button changed to red color
# version: 1.0
# authors: İbrahim AVCI


enabled_site_setting :name_mask_enabled

register_asset 'javascripts/discourse/helpers/name-mask.js.es6'
register_asset "javascripts/discourse/templates/components/user-info.hbs"







